package week2.day1.cw;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnAlert {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//set driver path
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		//launch browser
		ChromeDriver driver = new ChromeDriver();
	    //load URL
		driver.get("http://jqueryui.com/draggable/");
		//maximize
		driver.manage().window().maximize();
		WebElement frame = driver.findElementByClassName("demo-frame");
	    driver.switchTo().frame(frame);
		String text = driver.findElementById("draggable").getText();
		System.out.println(text);
		driver.switchTo().defaultContent();
	driver.findElementByLinkText("Download").click();
	}

}
