package week2.day3.cw.inheritance;

public class Car extends Vehicle{

	public void callVehicles() {
		applyBrake();
		soundHorn();
		System.out.println("callVehicles");
}
}
