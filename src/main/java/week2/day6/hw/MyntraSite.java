package week2.day6.hw;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;


public class MyntraSite {
	
	static RemoteWebDriver driver;
	static Actions builder;
	static WebDriverWait wait;
	@Test
	public  void main3 (){
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeOptions opt = new ChromeOptions();
		opt.addArguments("--disable-notifications");
		driver = new ChromeDriver(opt);
		wait = new WebDriverWait(driver, 20);
		builder = new Actions(driver);
		driver.manage().window().maximize();
		driver.get("https://www.myntra.com/search");
		driver.findElementByXPath("//input[@class ='desktop-searchBar']").sendKeys("Sunglasses");
		driver.findElementByXPath("//a[@class ='desktop-submit']").click();
		List<WebElement> ListOfBrandName = driver.findElementsByXPath("(//div[@class='product-productMetaInfo'][.//h4[contains(text(), 'Unisex')] and .//span[contains(text(), '50% OFF')]])/div[1]");
		List<WebElement> ListOfPrices = driver.findElementsByXPath("(//div[@class='product-productMetaInfo'][.//h4[contains(text(), 'Unisex')] and .//span[contains(text(), '50% OFF')]])/div[2]/span/span[@class='product-discountedPrice']");
		//		List<Integer> list1 = new ArrayList<Integer>();
	    for (int i=0; i < ListOfBrandName.size(); i++)
	    {
	    	System.out.println("Unisex item brand name is " + ListOfBrandName.get(i).getText()
	 +"  with 50% OFF discounted price is :"+ ListOfPrices.get(i).getText());
		}
		WebElement faceShape = driver.findElementByXPath("//h4[text()='Face Shape']");
		wait.until(ExpectedConditions.elementToBeClickable(faceShape));		
		faceShape.click();
	    WebElement round = driver.findElementByXPath("//input[@type='checkbox' and @value='round']");
		wait.until(ExpectedConditions.elementSelectionStateToBe(round, false));
		builder.moveToElement(round).click().perform();
		WebElement spinner = driver.findElementByClassName("loader-spinner");
		wait.until(ExpectedConditions.invisibilityOf(spinner));
		driver.findElementByXPath("//h4[text()='Type']").click();
		WebElement Oval = driver.findElementByXPath("//input[@type='checkbox' and @value='oval sunglasses']");
	    wait.until(ExpectedConditions.elementSelectionStateToBe(Oval, false));
		builder.moveToElement(Oval).click().perform();
		WebElement firstproduct = driver.findElementByXPath("(//div[@class='product-thumbShim']/following::a)[1]//img");
		wait.until(ExpectedConditions.elementToBeClickable(firstproduct));
		builder.moveToElement(firstproduct).pause(1000).click().perform();
		Set<String> allwindow = driver.getWindowHandles();
		List<String> List1 = new ArrayList<String>();
		List1.addAll(allwindow);
		driver.switchTo().window(List1.get(1));
		String productName = driver.findElementByXPath("//h1[@class='pdp-name']").getText();
		System.out.println("product name  "+ productName);
		WebElement addToBag = driver.findElementByXPath("//div[contains(text(), 'ADD TO BAG')]");
		addToBag.click();
		WebElement notification = driver.findElementByClassName("notify-info-message");
		wait.until(ExpectedConditions.visibilityOf(notification));
		driver.findElementByXPath("(//span[starts-with(@class, 'myntraweb')])[3]").click();
		String bagProductName = driver.findElementByXPath("//div[@class='seller']/preceding::div[1]/a").getText();
		System.out.println(bagProductName);
		if (bagProductName.contains(productName))
			System.out.println(productName + "product is available in to bag");
		else
			System.out.println("not into the bag");
	}

}
