package week2.day4.hw;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;

public class MoveElementAnotherLocation {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver ();			
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("http://jqueryui.com");
		driver.findElementByXPath("//a[text()='Sortable']").click();
		driver.switchTo().frame(0);
		WebElement web1 = driver.findElementByXPath("//li[text() = 'Item 1']");
		Point l1 = driver.findElementByXPath("//li[text() = 'Item 1']").getLocation();
		Point l5 = driver.findElementByXPath("//li[text() = 'Item 5']").getLocation();
		WebElement web6 = driver.findElementByXPath("//li[text() = 'Item 6']");
		Point l3 = driver.findElementByXPath("//li[text() = 'Item 3']").getLocation();
		Point l7 = driver.findElementByXPath("//li[text() = 'Item 7']").getLocation();
		System.out.println(l1);System.out.println(l5);
		System.out.println(l3);System.out.println(l7);
		Actions builder1 = new Actions (driver);
		//builder1.sendKeys(Keys.CONTROL).click(w1).click(w4).click(w7).perform();			
		builder1.dragAndDropBy(web1, 11, 171).perform();
		builder1.dragAndDropBy(web6, 11, 253).perform();
	}

}
