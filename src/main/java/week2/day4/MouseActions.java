package week2.day4;

import org.openqa.selenium.Keys;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class MouseActions {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver ();			
		driver.manage().window().maximize();
				driver.get("http://jqueryui.com/draggable/");
		driver.findElementByXPath("//a[text()='Draggable']").click();
		driver.switchTo().frame(0);
		WebElement drag = driver.findElementById("draggable");
		Point location = driver.findElementById("draggable").getLocation();
		System.out.println(location);
		Actions builder = new Actions(driver);
		builder.dragAndDropBy(drag, 30, 40).perform();
		Point location1 = driver.findElementById("draggable").getLocation();
		System.out.println("Moved location" +location1);
				driver.switchTo().defaultContent();
				driver.findElementByXPath("//a[text()='Selectable']").click();
				Thread.sleep(5000);
				driver.switchTo().frame(0);
				WebElement w1 = driver.findElementByXPath("//li[text() = 'Item 1']");
				WebElement w4 = driver.findElementByXPath("//li[text() = 'Item 4']");
				WebElement w7 = driver.findElementByXPath("//li[text() = 'Item 7']");
				Actions builder1 = new Actions (driver);
				builder1.sendKeys(Keys.CONTROL).click(w1).click(w4).click(w7).perform();
			
//		driver.close();
//		driver.quit();
//		
		
		
		
		
	}

}
