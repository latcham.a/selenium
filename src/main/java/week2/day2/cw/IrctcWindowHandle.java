package week2.day2.cw;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.chrome.ChromeDriver;

public class IrctcWindowHandle {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		String fwindowtile = driver.getTitle();
		System.out.println(fwindowtile);
		String fwinurl = driver.getCurrentUrl();
		System.out.println(fwinurl);
		driver.findElementByXPath("//a[text()='Contact Us']");
		Set<String> allwindow = driver.getWindowHandles();

		List<String> List1 = new ArrayList<String>();
		List1.addAll(allwindow);
		driver.switchTo().window(List1.get(1));
		String swindowtile = driver.getTitle();
		System.out.println(swindowtile);
		String swinurl = driver.getCurrentUrl();
		System.out.println(swinurl);
		driver.switchTo().window(List1.get(0));
		driver.close();
		System.out.println("closed");


	}

}
