package week2.day2.hw;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class CloseMeFrame {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://layout.jquery-dev.com/demos/iframe_local.html");

		//iframe inside  west
		WebElement webfram1 = driver.findElementById("childIframe");
		driver.switchTo().frame(webfram1);
		Thread.sleep(2000);
		driver.findElementByXPath("(//button[text()='Close Me'])[1]").click();
		System.out.println("iframe West Close me clcked");

		//iframe inside  ease
		Thread.sleep(2000);
		//East Frame
		driver.findElementByXPath("(//button[text()='Close Me'])[2]").click();
		System.out.println("iframe East Close me clcked");
		Thread.sleep(2000);
		//Default content west
		driver.switchTo().defaultContent();
		driver.findElementByXPath("(//button[text()='Close Me'])[1]").click();
		System.out.println("Default Content west Close me clcked");
		Thread.sleep(2000);
		//Default content east
		driver.findElementByXPath("(//button[text()='Close Me'])[2]").click();
		System.out.println("Default Content East Close me clcked");
		Thread.sleep(2000);
		driver.close();
		driver.quit();
	}

}
