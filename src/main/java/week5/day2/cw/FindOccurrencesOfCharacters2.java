package week5.day2.cw;

public class FindOccurrencesOfCharacters2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

//		 find occurrences of characters
		String str = "Amazon India";

		char[] array = str.toCharArray();
		int count = 0;
		for (char c : array) {
			if(c == 'A' || c == 'a') {
				count++;
			}
		}
		System.out.println(count);
		}

}
