package week5.day2.cw;

public class Stringclass {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String str1 = "test";  //String pool
		String str2 = "test";  //String pool
		System.out.println( System.identityHashCode(str1));
		System.out.println( System.identityHashCode(str2));
		String str3 = new String ("test");  //Heap
		String str4 = new String ("test");  //Heap
		System.out.println( System.identityHashCode(str3));
		System.out.println( System.identityHashCode(str4));
	}

}
