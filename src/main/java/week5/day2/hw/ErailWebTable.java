package week5.day2.hw;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ErailWebTable {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");

		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("https://erail.in/");
		WebElement from = driver.findElementById("txtStationFrom");
		from.clear();
		from.sendKeys("MAS", Keys.TAB);
		WebElement to = driver.findElementById("txtStationTo");
		to.clear();
		to.sendKeys("SBC", Keys.TAB);
		driver.findElementById("chkSelectDateOnly").click();

		WebElement table = 
				driver.findElementByXPath("//table[@class='DataTable TrainList']");

		List<WebElement> trs = table.
				findElements(By.tagName("tr"));
		System.out.println(trs.size());
		for (WebElement rows : trs) {
			List<WebElement> cols = rows.
					findElements(By.tagName("td"));
//			cols.sort(c);
			System.out.println(cols.get(1).getText());
}
	}
//Collection<E>
}
