package week1.day2;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LeafTapsLogin {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps");
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys("Lax International");
		driver.findElementById("createLeadForm_firstName").sendKeys("Latcham");
		driver.findElementById("createLeadForm_lastName").sendKeys("Appasamy");
      driver.findElementByName("submitButton").click();
      WebDriverWait  wait = new WebDriverWait  (driver, 50000);
      wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("submitButton")));
           String compNameVerify = driver.findElementById("viewLead_companyName_sp").getText();
           
           System.out.println(compNameVerify);
      if (compNameVerify.equals("Lax International"))
    	 
      {
    	  System.out.println("equal contition verified \n");
      }
      else 
      {
    	  System.out.println("equal not satisfied \n");
      }
      if (compNameVerify.contains("Lax"))
      {
    	  System.out.println("contains verified \n");
      }
      else 
      {
    	  System.out.println("contains condition is not satisfied \n");
      }
      if (compNameVerify.equalsIgnoreCase("lax international"))
      {
    	  System.out.println("ignore case verrified \n");
      }
      else
      {
    	  System.out.println("case ignore condition is not satisfied \n");
      }
      String fNameVerify = driver.findElementById("viewLead_firstName_sp").getText();
      if (fNameVerify.equals("Latcham"))
    	  System.out.println("fname verifieed");
      String lNameverify = driver.findElementById("viewLead_lastName_sp").getText();
      if (lNameverify.equals("Appasamy"))
      System.out.println("Last name verified \n");

     
	}

}
