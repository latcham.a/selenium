package week1.day2;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomeworkOptionalLeafTapsLogin {


	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		// chrome driver
System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	ChromeDriver driver = new ChromeDriver ();
		
	// IE driver
		/*System.setProperty("webdriver.ie.driver", "./drivers/IEDriverServer.exe");
		InternetExplorerDriver driver = new InternetExplorerDriver ();
		*/
		// mozilla
		
//	System.setProperty("webdriver.gecko.driver", "./drivers/chromedriver.exe");
//		FirefoxDriver driver = new FirefoxDriver();
		
		// driver initialization completed
		
	driver.manage().window().maximize();
	
	driver.get("http://leaftaps.com/opentaps");
//	  WebDriverWait  wait = new WebDriverWait  (driver, 5000);
//      wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("username")));
	driver.findElementById("username").sendKeys("DemoSalesManager");
	driver.findElementById("password").sendKeys("crmsfa");
	driver.findElementByClassName("decorativeSubmit").click();
	driver.findElementByLinkText("CRM/SFA").click();
	driver.findElementByLinkText("Leads").click();
	driver.findElementByLinkText("Create Lead").click();
	//
	driver.findElementById("createLeadForm_companyName").sendKeys("Hari Iinternationals ltc");
	driver.findElementById("createLeadForm_firstName").sendKeys("Hari");
	driver.findElementById("createLeadForm_lastName").sendKeys("Latcham");
	driver.findElementById("createLeadForm_dataSourceId").sendKeys("Cold Call");
  driver.findElementById("createLeadForm_firstNameLocal").sendKeys("Hariahara");
  driver.findElementById("createLeadForm_personalTitle").sendKeys("Mr. ");
  driver.findElementById("createLeadForm_generalProfTitle").sendKeys("Welcome");
  driver.findElementById("createLeadForm_annualRevenue").sendKeys("600000.00");
  driver.findElementById("createLeadForm_industryEnumId").sendKeys("Computer Software");
  driver.findElementById("createLeadForm_ownershipEnumId").sendKeys("OWN_PROPRIETOR");
  driver.findElementById("createLeadForm_sicCode").sendKeys("SIC CODE1");
  driver.findElementById("createLeadForm_description").sendKeys("Decription entered by latcham");
  driver.findElementById("createLeadForm_importantNote").sendKeys("One Note entered by latchamm");
driver.findElementById("createLeadForm_marketingCampaignId").sendKeys("Marketcapaign1");
driver.findElementById("createLeadForm_lastNameLocal").sendKeys("Appasamay");
driver.findElementByXPath("//img[@id ='createLeadForm_birthDate-button']").click();
driver.switchTo().frame("org.opentaps.gwt.crmsfa.crmsfa");
System.out.println("i am into frame");
driver.findElementByXPath("//td [contains(text(), '15')]").click();
driver.switchTo().defaultContent();
driver.findElementById("createLeadForm_departmentName").sendKeys("Agri");
driver.findElementById("createLeadForm_currencyUomId").sendKeys("INR - Indian Rupee"); 
driver.findElementById("createLeadForm_numberEmployees").sendKeys("100");
driver.findElementById("createLeadForm_tickerSymbol").sendKeys("$");

// contact Informaiton
 driver.findElementById("createLeadForm_primaryPhoneCountryCode").sendKeys("91");
 driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("044");
 driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("1234");
 driver.findElementById("createLeadForm_primaryEmail").sendKeys("latcham.appasamy@gmail.com");
 driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("9094020939");
 // family info
 driver.findElementById("createLeadForm_primaryPhoneAskForName").sendKeys("Hari");
 driver.findElementById("createLeadForm_primaryWebUrl").sendKeys("weburl.com");
 driver.findElementById("createLeadForm_generalToName").sendKeys("Hari");
 driver.findElementById("createLeadForm_generalAttnName").sendKeys("Hari"); 	
 driver.findElementById("createLeadForm_generalAddress1").sendKeys("#133 veerapandian st");
driver.findElementById("createLeadForm_generalAddress2").sendKeys("veerapuram");
driver.findElementById("createLeadForm_generalCity").sendKeys("chennai");

driver.findElementById("createLeadForm_generalPostalCode").sendKeys("603 002");
driver.findElementById("createLeadForm_generalCountryGeoId").sendKeys("India");
driver.findElementById("createLeadForm_generalPostalCodeExt").sendKeys("123");
Thread.sleep(2000);
driver.findElementById("createLeadForm_generalStateProvinceGeoId").sendKeys("TAMILNADU");
//driver.findElementByName("submitButton").click();

 System.out.println("lead created successfully");
	}


}

