package week1.day4.hw;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnFrame {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver ();
		driver.manage().window().maximize();
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		WebElement webfram1 = driver.findElementById("iframeResult");
		driver.switchTo().frame(webfram1);
		
		driver.findElementByXPath("//button[text()='Try it']").click();
		

		String alertbeforetext = driver.switchTo().alert().getText();
		System.out.println(alertbeforetext);
		
		driver.switchTo().alert().sendKeys("latcham");
		driver.switchTo().alert().accept();
		String hellotext = driver.findElementByXPath("//p[@id='demo']").getText();

		if (hellotext.contains("latcham"))
		{
			System.out.println("entered text is verified and matched");
		}
		else
		{
			System.out.println("entered text vereifiied and not matched");
		}

	}




}


