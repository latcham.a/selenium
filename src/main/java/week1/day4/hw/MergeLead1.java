package week1.day4.hw;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MergeLead1 {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		WebDriverWait wait = new WebDriverWait (driver, 10);
		driver.get("http://leaftaps.com/opentaps");
		driver.findElementByXPath("//input[@name = 'USERNAME']").sendKeys("DemoSalesManager");
		driver.findElementByXPath("//input[@name ='PASSWORD']").sendKeys("crmsfa");
		driver.findElementByXPath("//input[@class='decorativeSubmit']").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByXPath("//a[text() ='Leads']").click();
		driver.findElementByXPath("//a[text()='Merge Leads']").click();
		driver.findElementByXPath("//img[@src='/images/fieldlookup.gif']").click();
		Set<String> fromLead = driver.getWindowHandles();
		List<String>  Listfromlead = new ArrayList<String>();
		Listfromlead.addAll(fromLead);
		System.out.println("before switch URL is" +driver.getCurrentUrl());
		driver.switchTo().window(Listfromlead.get(1));
		System.out.println("current URL is" +driver.getCurrentUrl());
		driver.findElementByXPath("(//input[contains(@class, 'x-form-text x-form-field')])[2]").sendKeys("Lax");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
//		Thread.sleep(5000);
//       WebElement OneMoment = driver.findElementByXPath("//script[contains(text(), 'One Moment...')]");
//		wait.until(ExpectedConditions.invisibilityOf(OneMoment));
		Thread.sleep(500);
		WebElement OneMoment = driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]");
		wait.until(ExpectedConditions.elementToBeClickable(OneMoment));
		String leadid1 = driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]").getText();
		System.out.println(leadid1);
		WebElement firstProduct = driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
//		Actions builder = new Actions(driver);
//		builder.moveToElement(firstProduct).pause(1000).click().perform();
		wait.until(ExpectedConditions.elementToBeClickable(firstProduct));
		firstProduct.click();
		driver.switchTo().window(Listfromlead.get(0));

		driver.findElementByXPath("(//img[@src='/images/fieldlookup.gif'])[2]").click();
		Set<String> toLead = driver.getWindowHandles();
		List<String>  Listtolead = new ArrayList<String>();
		Listtolead.addAll(toLead);
		System.out.println("before switch URL is" +driver.getCurrentUrl());
		driver.switchTo().window(Listtolead.get(1));
		System.out.println("current URL is" +driver.getCurrentUrl());
		driver.findElementByXPath("(//input[contains(@class, 'x-form-text x-form-field')])[2]").sendKeys("Hari");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(5000);
		String leadid2 = driver.findElementByXPath("(//a[contains(@class, 'linktext')])[1]").getText();
		driver.findElementByXPath("(//a[contains(@class, 'linktext')])[1]").click();
		driver.switchTo().window(Listfromlead.get(0));
		driver.findElementByXPath("//a[@class='buttonDangerous']").click();
		driver.switchTo().alert().accept();
		//Thread.sleep(5000);
		driver.findElementByXPath("//a[text() ='Find Leads']").click();
		System.out.println("lead" +leadid1);
		driver.findElementByXPath("(//input[contains(@class, 'x-form-text x-form-field')])[28]").sendKeys(leadid1);

		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(2000);
		String text1 = driver.findElementByXPath("//div[@class ='x-paging-info']").getText();
		if (text1.contains("No"))
			System.out.println(leadid1 +"not found");
		else
		System.out.println(leadid1 +"found");
		driver.findElementByXPath("(//input[contains(@class, 'x-form-text x-form-field')])[28]").clear();
		driver.findElementByXPath("(//input[contains(@class, 'x-form-text x-form-field')])[28]").sendKeys(leadid2);

		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(2000);
		String text2 = driver.findElementByXPath("//div[@class ='x-paging-info']").getText();
		if (text2.contains("No"))
			System.out.println(leadid2 +"not found");
		else
			System.out.println(leadid2 +" found");
		System.out.println("leads merged successfully to "+ leadid2);
		driver.close();
		driver.quit();



	}

}
