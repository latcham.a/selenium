package TestNGGroups;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import testcases.ProjectSpecificMethod;
import testcases.Tc001_LoginAndLogout;

public class CreateLeads  extends ProjectSpecificMethod {
	

@Test (groups = "smoke")
	public void createLead() throws Exception
	{		

		locateElement("LinkText", "Create Lead").click();
		WebElement cName = locateElement("id", "createLeadForm_companyName");
		type(cName, "Lax Iinternationals ltc");		
		WebElement fName = locateElement("id", "createLeadForm_firstName");
		type(fName, "Hari");
		WebElement lName = locateElement("id", "createLeadForm_lastName");
		type(lName, "lax");
		WebElement drop1 = locateElement("id", "createLeadForm_dataSourceId");
		selectDropDownUsingText(drop1, "Conference");
		WebElement title = locateElement("id", "createLeadForm_personalTitle");
		type(title, "Mr.");		
		WebElement leadtittle = locateElement("id", "createLeadForm_generalProfTitle");
		type(leadtittle, "Hari");
		WebElement revenue = locateElement("id", "createLeadForm_annualRevenue");
		type(revenue, "600000.00");
		WebElement WebelementIndustry = locateElement("id", "createLeadForm_industryEnumId");
		selectDropDownUsingText(WebelementIndustry, "Retail");
		WebElement webelementOwnerShip = locateElement("id", "createLeadForm_ownershipEnumId");
		selectDropDownUsingText(webelementOwnerShip, "Partnership");
		WebElement sitcode = locateElement("id", "createLeadForm_sicCode");
		type(sitcode, "SIC CODE1");
		WebElement dec = locateElement("id", "createLeadForm_description");
		type(dec, "Decription entered by latcham");
		WebElement note = locateElement("id", "createLeadForm_importantNote");
		type(note, "createLeadForm_importantNote");
		WebElement mktcapaign = locateElement("id", "createLeadForm_marketingCampaignId");
		selectDropDownUsingText(mktcapaign, "Automobile");
		WebElement llname = locateElement("id", "createLeadForm_lastNameLocal");
		type(llname, "Appasamay");
		WebElement deptname = locateElement("id", "createLeadForm_departmentName");
		type(deptname, "Agri");
		WebElement csymbole = locateElement("id", "createLeadForm_currencyUomId");
		selectDropDownUsingText(csymbole, "INR - Indian Rupee");
		WebElement emp = locateElement("id", "createLeadForm_numberEmployees");
		type(emp, "100");
		WebElement sym = locateElement("id", "createLeadForm_tickerSymbol");
		type(sym, "$");

		WebElement ccode = locateElement("id", "createLeadForm_primaryPhoneCountryCode");
		type(ccode, "91");
		WebElement pacode = locateElement("id", "createLeadForm_primaryPhoneAreaCode");
		type(pacode, "044");
		WebElement extn = locateElement("id", "createLeadForm_primaryPhoneExtension");
		type(extn, "1234");
		WebElement email = locateElement("id", "createLeadForm_primaryEmail");
		type(email, "latcham.appasamy@gmail.com");
		WebElement phoneno = locateElement("id", "createLeadForm_primaryPhoneNumber");
		type(phoneno, "9094020939");
		// family info
		WebElement forName = locateElement("id", "createLeadForm_primaryPhoneAskForName");
		type(forName, "Hari");
		WebElement url = locateElement("id", "createLeadForm_primaryWebUrl");
		type(url, "weburl.com");
		WebElement toname = locateElement("id", "createLeadForm_generalToName");
		type(toname, "Hari");
		WebElement aname = locateElement("id", "createLeadForm_generalAttnName");
		type(aname, "Hari");
		WebElement add1 = locateElement("id", "createLeadForm_generalAddress1");
		type(add1, "#133 veerapandian st");
		WebElement add2 = locateElement("id", "createLeadForm_generalAddress2");
		type(add2, "veerapuram");
		WebElement city = locateElement("id", "createLeadForm_generalCity");
		type(city, "chennai");
		WebElement pin = locateElement("id", "createLeadForm_generalPostalCode");
		type(pin, "603 002");
		WebElement country = locateElement("id", "createLeadForm_generalCountryGeoId");
		selectDropDownUsingText(country, "India");
		WebElement pinextn = locateElement("id", "createLeadForm_generalPostalCodeExt");
		type(pinextn, "123");
		WebElement state = locateElement("id", "createLeadForm_generalStateProvinceGeoId");
		selectDropDownUsingText(state, "TAMILNADU");
		WebElement submitbtn = locateElement("name", "submitButton");
		click(submitbtn);
			
	}
}