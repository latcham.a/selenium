package TestNGGroups;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import testcases.ProjectSpecificMethod;

public class DeleteLeads extends ProjectSpecificMethod {

	
@Test /*(groups = "reg",  dependsOnGroups ="smoke")*/
public void main () throws Exception {
WebElement Leads = locateElement("xpath", "//a[text() ='Leads']");
click(Leads);
WebElement Leads2 = locateElement("xpath", "//a[text() ='Leads']");
click(Leads2);
WebElement FindLead = locateElement("xpath", "//a[text() ='Find Leads']");
click(FindLead);
WebElement fname = locateElement("xpath", "(//input[contains(@class, 'x-form-text x-form-field')])[29]");
type(fname, "Hari");
WebElement btnFindLead = locateElement("xpath", "//button[text()='Find Leads']");
click(btnFindLead);		
//Thread.sleep(5000);
WebElement firstRecodLink = locateElement("xpath", "(//a[contains(@class, 'linktext')])[4]");
waitTillClickabe(firstRecodLink);
String leadid = getText(firstRecodLink);
System.out.println(leadid);
click(firstRecodLink);
WebElement deleteBtn = locateElement("xpath", "(//a[contains(@class, 'subMenuButton')])[4]");
click(deleteBtn);
WebElement Leads3 = locateElement("xpath", "//a[text() ='Leads']");
//Thread.sleep(5000);
waitTillClickabe(Leads3);
click(Leads3);
WebElement FindLead1 = locateElement("xpath", "//a[text() ='Find Leads']");
click(FindLead1);
WebElement leadidForFind = locateElement("xpath", "(//input[contains(@class, 'x-form-text x-form-field')])[28]");
type(leadidForFind, leadid);
WebElement btnFindLead1 = locateElement("xpath", "//button[text()='Find Leads']");
click(btnFindLead1);		
//Thread.sleep(5000);
 WebElement noTextLoc = locateElement("xpath", "//div[text()='No records to display']");
 waitTillClickabe(noTextLoc);
 String norecordtxt = getText(noTextLoc);
if (norecordtxt.contains("No"))
System.out.println("record deleted successfully");
	}

	}

