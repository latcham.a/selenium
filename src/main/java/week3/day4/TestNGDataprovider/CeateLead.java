package week3.day4.TestNGDataprovider;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import utils.ReadExcel;
import week3.day2.TestNGGROUPS.PSM;

public class CeateLead extends PSM{
	
	@Test(/*groups= {"smoke"}, *//*dataProviderClass=GetData.class,*/
			dataProvider="fetchData")
	public void createLead(String acname, String afname, String alname) {
		locateElement("LinkText", "Create Lead").click();
		WebElement cName = locateElement("id", "createLeadForm_companyName");
		type(cName, acname);		
		WebElement fName = locateElement("id", "createLeadForm_firstName");
		type(fName, afname);
		WebElement lName = locateElement("id", "createLeadForm_lastName");
		type(lName, alname);
		WebElement submitbtn = locateElement("name", "submitButton");
		click(submitbtn);
	}
	/*@DataProvider(name="fetchData")
	public String[][] dynamicData() {
		String[][] data = new String[2][3];
		   data[0][0] = "TL";
		   data[0][1] = "Koushik";
		   data[0][2] = "Ch";
		   
		   data[1][0] = "TL";
		   data[1][1] = "Gopi";
		   data[1][2] = "J";
		   return data;
	}*/
    @DataProvider(name="fetchData")
	public String[][] getData() throws IOException {
		String[][] data = ReadExcel.readExcel("CreateLead");
return data;
}
}
