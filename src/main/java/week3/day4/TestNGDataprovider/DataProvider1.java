package week3.day4.TestNGDataprovider;

import org.openqa.selenium.WebElement;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import week3.day2.TestNGGROUPS.PSM;

public class DataProvider1 extends 	PSM {
	@Test (dataProvider="CLData",threadPoolSize=2, invocationCount=3)
		public void createLead (String cname, String fname, String lname)
		{
		locateElement("LinkText", "Create Lead").click();
		WebElement cName = locateElement("id", "createLeadForm_companyName");
		type(cName, cname);		
		WebElement fName = locateElement("id", "createLeadForm_firstName");
		type(fName, fname);
		WebElement lName = locateElement("id", "createLeadForm_lastName");
		type(lName, lname);
		WebElement submitbtn = locateElement("name", "submitButton");
		click(submitbtn);
		}
	
   @DataProvider (name =  "CLData")
	public String[][] dynamic() {
		// TODO Auto-generated constructor stub
		String [][] data = new String [2][3];
		data [0][0] = "Lax International";
		data [0][1] = "Latcham";
		data [0][2] = "Appasamy";
		
		data [1][0] = "Hari International";
		data [1][1] = "Hari";
		data [1][2] = "Latcham";
		return data;
		
	}
}

