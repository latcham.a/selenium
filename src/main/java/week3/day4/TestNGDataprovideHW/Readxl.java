package week3.day4.TestNGDataprovideHW;

import java.io.IOException;
import java.util.Iterator;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Readxl {
	
	public String[][] readexcel(String fname) throws IOException
	{
//open excel wb
		System.out.println("opening the xl");
		XSSFWorkbook WB = new XSSFWorkbook("./data/"+fname+".xlsx");
//goto sheet
		XSSFSheet sheet1 = WB.getSheetAt(0);
//go to row
		int rownum = sheet1.getLastRowNum();
		System.out.println(rownum);
		int colcount = sheet1.getRow(0).getLastCellNum();
		System.out.println(colcount);
		String [][] data = new String [rownum][colcount];
		
		for (int i = 1; i <= rownum; i++) {
			XSSFRow ROW = sheet1.getRow(i);	
		
		//go to cell
	
                 for (int j = 0; j < colcount; j++) {
			XSSFCell column= ROW.getCell(j);
			data [i-1][j] = column.getStringCellValue();
					
		}
             
		}

return data;

	}

}
