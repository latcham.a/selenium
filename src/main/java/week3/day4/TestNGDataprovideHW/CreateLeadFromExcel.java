package week3.day4.TestNGDataprovideHW;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import week3.day2.TestNGGROUPS.PSM;

public class CreateLeadFromExcel extends PSM{
	
	@Test  (dataProvider = "getdata")
	public void createLead (String acname, String afname, String alname)
	{
		locateElement("LinkText", "Create Lead").click();
		WebElement cName = locateElement("id", "createLeadForm_companyName");
		type(cName, acname);		
		WebElement fName = locateElement("id", "createLeadForm_firstName");
		type(fName, afname);
		WebElement lName = locateElement("id", "createLeadForm_lastName");
		type(lName, alname);
		WebElement submitbtn = locateElement("name", "submitButton");
		click(submitbtn);
	}
	
	@DataProvider (name = "getdata")
	public String[][] exceldata() throws IOException
	{
		Readxl data = new Readxl ();
		
		String[][] xceldata = data.readexcel("CreateLead");
			
			return xceldata;
		
		
				
	}

}
