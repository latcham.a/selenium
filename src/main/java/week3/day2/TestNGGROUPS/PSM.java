package week3.day2.TestNGGROUPS;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

import wdmethods.SeMethods;

public class PSM extends SeMethods{
	@Parameters ({"url", "browser", "uname","pw"})
	@BeforeMethod(groups="common")
	public void login(String url, String browser, String username, String Password) {
		startApp(browser, url);
		WebElement eleUsername = locateElement("id", "username");
		type(eleUsername, username);
		WebElement elePassword = locateElement("id", "password");
		type(elePassword, Password);
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		WebElement elecrmsfa = locateElement("LinkText", "CRM/SFA");
		click(elecrmsfa);
	
	}
	@AfterMethod(groups="common")
		public void close() {
		closeAllBrowsers();
	}
}
