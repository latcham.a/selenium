package week4.day1.ExceptionHanding.ExtendReports;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class ExtendReports {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ExtentHtmlReporter Html =new ExtentHtmlReporter("./reports/report.html");
		Html.setAppendExisting(true);
		ExtentReports extend = new ExtentReports();
		extend.attachReporter(Html);
		ExtentTest Test = extend.createTest("tc001", "login Verification");
		Test.assignAuthor("Latcham");
		Test.assignCategory("Secutiry");
		Test.pass("Entter user name");
		Test.pass("Entter pw");
		Test.pass("Clicked login button");
		extend.flush();
				
			}

}
