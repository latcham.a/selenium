package testcases;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.chrome.ChromeDriver;

public class javascriptbrowserload {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		//Load URL		
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps");

		//get the state msg
		JavascriptExecutor js = (JavascriptExecutor)driver;	
		String states = js.executeScript("return document.readyState").toString();

		//verification
		switch (states) {
		case "loading": System.out.println("The document is still loading.");			
		break;

		case "interactive": System.out.println("The document has finished loading.But sub-resources such as images, stylesheets and frames are still loading.");			
		break;

		case "complete": System.out.println("The page is fully loaded.");	
		break;

		default:
		break;
		}		
			
	}

}
