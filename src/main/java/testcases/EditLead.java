package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;


public class EditLead extends Tc001_LoginAndLogout{


@Test
	public  void EditLead () throws Exception{
		login();
		Thread.sleep(5000);
		WebElement web1 = locateElement("LinkText", "CRM/SFA");
		click(web1);
		WebElement Leads = locateElement("xpath", "//a[text() ='Leads']");
		click(Leads);
		WebElement Leads2 = locateElement("xpath", "//a[text() ='Leads']");
		click(Leads2);
		WebElement FindLead = locateElement("xpath", "//a[text() ='Find Leads']");
		click(FindLead);
		WebElement fname = locateElement("xpath", "(//input[contains(@class, 'x-form-text x-form-field')])[29]");
		type(fname, "Lax");
		WebElement btnFindLead = locateElement("xpath", "//button[text()='Find Leads']");
		click(btnFindLead);		
		Thread.sleep(5000);
		WebElement flead = locateElement("xpath", "(//a[contains(@class, 'linktext')])[4]");
		click(flead);		
		WebElement Editbutton = locateElement("xpath", "(//a[contains(@class, 'subMenuButton')])[3]");
		click(Editbutton);	
		WebElement compName = locateElement("xpath", "(//input[contains(@class, 'inputBox')])[1]");
		clear(compName);	
		type(compName, "updated by lax");
		WebElement updateButton = locateElement("xpath", "(//input[contains(@class, 'smallSubmit')])[1]");
		click(updateButton);			
		String viewCompanyName = locateElement("xpath", "(//span[contains(@id, 'viewLead_companyName_sp')])[1]").getText();
		System.out.println(viewCompanyName);
//			String updatedCompanyName = getText(viewCompanyName);
//		System.out.println(updatedCompanyName);
		if (viewCompanyName.contains("updated by lax"))
			System.out.println("company name updated successfully");
		else
			System.out.println("not able to update company name");
		closeBrowser();
	
	}
}
