package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class MergeLeads1 extends Tc001_LoginAndLogout {

@Test 
	public void mergeLead() throws InterruptedException {					
		login();

		WebElement web1 = locateElement("LinkText","CRM/SFA");
		click(web1);
		WebElement Leads = locateElement("xpath", "//a[text() ='Leads']");
		click(Leads);
		WebElement mergeLeadlink = locateElement("xpath", "//a[text()='Merge Leads']");
		click(mergeLeadlink);
		WebElement firstImage = locateElement("xpath", "//img[@src='/images/fieldlookup.gif']");
		click(firstImage);
		switchToWindow(1);
		WebElement fname = locateElement("xpath", "(//input[contains(@class, 'x-form-text x-form-field')])[2]");
		type(fname,"latcham");
		WebElement findLead1 = locateElement("xpath", "//button[text()='Find Leads']");
		click(findLead1);
		Thread.sleep(3000);
		WebElement leadid1loc = locateElement("xpath", "(//a[contains(@class, 'linktext')])[1]");
		String leadid1 = leadid1loc.getText();
		click(leadid1loc);
		switchToWindow(0);
		WebElement findLeadimg2 = locateElement("xpath", "(//img[@src='/images/fieldlookup.gif'])[2]");
		click(findLeadimg2);
		switchToWindow(1);
		WebElement fname1 = locateElement("xpath", "(//input[contains(@class, 'x-form-text x-form-field')])[2]");
		type(fname1, "Hari");
		WebElement findLead2 = locateElement("xpath", "//button[text()='Find Leads']");
		click(findLead2);
		Thread.sleep(3000);
		WebElement leadid2loc = locateElement("xpath", "(//a[contains(@class, 'linktext')])[1]");
		String leadid2 = leadid2loc.getText();
		click(leadid2loc);
		switchToWindow(0);
		WebElement mergeBtn = locateElement("xpath", "//a[@class='buttonDangerous']");
		click(mergeBtn);
		acceptAlert();
		WebElement findLeadLink = locateElement("xpath", "//a[text() ='Find Leads']");
		click(findLeadLink);
		System.out.println("lead" +leadid1);
		WebElement leadtextbox = locateElement("xpath", "(//input[contains(@class, 'x-form-text x-form-field')])[28]");
		type(leadtextbox,leadid1);
		WebElement findLeadbtn2 = locateElement("xpath", "//button[text()='Find Leads']");
	    Thread.sleep(3000);
		click(findLeadbtn2);
		WebElement norecordtxt = locateElement("xpath", "//div[@class ='x-paging-info']");
		String text1 = norecordtxt.getText();	
		
		if (text1.contains("No"))
			System.out.println(leadid1 +"not found");
		else
			System.out.println(leadid1 +"found");
		WebElement leadtextbox2 = locateElement("xpath", "(//input[contains(@class, 'x-form-text x-form-field')])[28]");
		leadtextbox2.clear();
		type(leadtextbox2, leadid2);
		WebElement findLead3 = locateElement("xpath", "//button[text()='Find Leads']");
		Thread.sleep(3000);
		click(findLead3);
		WebElement norecordtxt2 = locateElement("xpath", "//div[@class ='x-paging-info']");
			String text2 =norecordtxt2.getText();
		if (text2.contains("No"))
			System.out.println(leadid2 +"not found");
		else
			System.out.println(leadid2 +" found");
		System.out.println("leads merged successfully to "+ leadid2);
		closeBrowser();
	



	}




}

