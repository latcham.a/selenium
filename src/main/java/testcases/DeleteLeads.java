package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class DeleteLeads extends Tc001_LoginAndLogout {

	
@Test
public void main () throws Exception {
// TODO Auto-generated method stub
login();
WebElement web1 = locateElement("LinkText", "CRM/SFA");
click(web1);
WebElement Leads = locateElement("xpath", "//a[text() ='Leads']");
click(Leads);
WebElement Leads2 = locateElement("xpath", "//a[text() ='Leads']");
click(Leads2);
WebElement FindLead = locateElement("xpath", "//a[text() ='Find Leads']");
click(FindLead);
WebElement fname = locateElement("xpath", "(//input[contains(@class, 'x-form-text x-form-field')])[29]");
type(fname, "Hari");
WebElement btnFindLead = locateElement("xpath", "//button[text()='Find Leads']");
click(btnFindLead);		

Thread.sleep(5000);
String leadid = locateElement("xpath", "(//a[contains(@class, 'linktext')])[4]").getText();
WebElement firstRecodLink = locateElement("xpath", "(//a[contains(@class, 'linktext')])[4]");
click(firstRecodLink);
WebElement deleteBtn = locateElement("xpath", "(//a[contains(@class, 'subMenuButton')])[4]");
click(deleteBtn);
Thread.sleep(5000);
click(Leads2);
click(FindLead);
WebElement leadidForFind = locateElement("xpath", "(//input[contains(@class, 'x-form-text x-form-field')])[28]");
type(leadidForFind, leadid);
click(btnFindLead);		
Thread.sleep(5000);
String norecord = locateElement("xpath", "//div[text()='No records to display']").getText();
if (norecord.contains("No"))
System.out.println("record deleted successfully");
closeBrowser();
	}

	}

