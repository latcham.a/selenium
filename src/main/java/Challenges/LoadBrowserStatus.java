package Challenges;

import org.openqa.selenium.chrome.ChromeDriver;

public class LoadBrowserStatus {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://developer.mozilla.org/en-US/docs/Web/API/Document/readyState");
	String status = driver.executeScript("return document.readyState").toString();
System.out.println(status);
	}

}
