package Challenges;

import java.awt.AWTException;
import java.awt.HeadlessException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;


import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class AlertSnapUsingRobotClass {

	@Test
	public void takeSnap() throws HeadlessException, AWTException, IOException, InterruptedException{
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();	
		driver.manage().window().maximize();
		driver.get("https://www.irctc.co.in/eticketing/forgotPassword.jsf");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	
		driver.findElementById("forgot_passwrd:checkDetails1").click();
		Thread.sleep(1000);
		
		// take snap		
		BufferedImage image = new Robot().createScreenCapture
		(new Rectangle(
				Toolkit.getDefaultToolkit().getScreenSize()));
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH mm ss");
		Date data = new Date();
		String TS=  dateFormat.format(data);
	
		ImageIO.write(image, "png", new File("./snapsalert/"+TS+" Alert.png"));
	
	}

	
	

}
