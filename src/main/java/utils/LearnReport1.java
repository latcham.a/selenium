
package utils;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class LearnReport1 {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		//ClassName obj = new ClassName();
		//path of report
		//Before suite 
		DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy HH mm ss");
		Date data = new Date();
		String TS=  dateFormat.format(data);
		ExtentHtmlReporter html = new ExtentHtmlReporter("./reports/"+TS+" result.html");
		//		html.setAppendExisting(true);
		ExtentReports extent = new ExtentReports();
		//attach report
		extent.attachReporter(html);
		//Before Class
		ExtentTest test = extent.createTest("TC001_Login", "Login into Leaftaps");
		test.assignAuthor("Gayatri");
		test.assignCategory("Smoke");
		//syso statement 
		test.pass("Enter username successfully", MediaEntityBuilder.createScreenCaptureFromPath("").build());
		test.pass("Enter password successfully");
		test.fail("Click login not successfully");
		//generate report
		//After suite
		extent.flush();
	}

}
